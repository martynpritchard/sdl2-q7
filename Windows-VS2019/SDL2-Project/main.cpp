//For exit()
#include <stdlib.h>
//For printf()
#include <stdio.h>
#if defined(_WIN32) || defined(_WIN64)
//The SDL library
#include "SDL.h"
//Support for loading different types of images.
#include "SDL_image.h"
#else
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#endif

//Global Variables
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

const int SDL_OK = 0;

//Bomb state
enum BombState { NORMAL = 0, BOOM = 1 };


int main(int argc, char* args[])
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface* temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture* backgroundTexture = nullptr;
    SDL_Texture* bombTexture = nullptr;
    SDL_Texture* playerTexture = nullptr;

    // Bomb Image source and target.
    const int BOMB_SPRITE_WIDTH = 16;
    const int BOMB_SPRITE_HEIGHT = 16;
    SDL_Rect unexploded; // first sprite of the sheet
    SDL_Rect exploded; // last sprite of the sheet.
    SDL_Rect targetRectangle;
    BombState bombState = NORMAL;

    // Window control
    SDL_Event event;
    bool quit = false; //false
    // Mouse coordinates
    int x = 0;
    int y = 0;


    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if (sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
        SDL_WINDOWPOS_UNDEFINED, // X position
        SDL_WINDOWPOS_UNDEFINED, // Y position
        WINDOW_WIDTH,            // width
        WINDOW_HEIGHT,           // height               
        SDL_WINDOW_SHOWN);       // Window flags



    if (gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if (gameRenderer == nullptr)
        {
            printf("Error - SDL could not create renderer\n");
            exit(1);
        }
    }

    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }


    /**********************************
     *    Setup background image     *
     * ********************************/

     // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");

    if (temp == nullptr)
    {
        printf("Background image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;


    /**********************************
     *    Setup bomb image     *
     * ********************************/

     // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/bomb_party_v4.png");

    if (temp == nullptr)
    {
        printf("Bomb image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    bombTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    targetRectangle.x = 250;
    targetRectangle.y = 250;
    targetRectangle.w = BOMB_SPRITE_WIDTH;
    targetRectangle.h = BOMB_SPRITE_HEIGHT;
    unexploded.x = 4 * BOMB_SPRITE_WIDTH;
    unexploded.y = 18 * BOMB_SPRITE_HEIGHT;
    unexploded.w = BOMB_SPRITE_WIDTH;
    unexploded.h = BOMB_SPRITE_HEIGHT;
    exploded.x = 14 * BOMB_SPRITE_WIDTH;
    exploded.y = 18 * BOMB_SPRITE_HEIGHT;
    exploded.w = BOMB_SPRITE_WIDTH;
    exploded.h = BOMB_SPRITE_HEIGHT;

    // Event loop
    while (!quit) // while quite is not true
    {
        // Handle input
        if (SDL_PollEvent(&event)) // poll for events
        {
            switch (event.type)
            {
            case SDL_QUIT: //If the user has closed(x)
            //out the window
            //Quit the program
                quit = true;
                break;
                //More events to come :-D

     // Note: This is not fully indented ... to wide for page :-(
            case SDL_MOUSEBUTTONDOWN: //If the left mouse button was pressed
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    //Get the mouse offsets
                    x = event.button.x;
                    y = event.button.y;
                    //If the mouse is over the button
                    if ((x > targetRectangle.x) &&
                        (x < targetRectangle.x + BOMB_SPRITE_WIDTH))
                    {
                        if ((y > targetRectangle.y) &&
                            (y < targetRectangle.y + BOMB_SPRITE_HEIGHT))
                        {
                            //Set the button sprite
                            bombState = BOOM;
                        }
                    }
                }
                break;
            default:
                // not an error, ignoring loads of other events.
                break;
            }
        }

    //Draw stuff here.
    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha
    // (transparency) values (ie. RGBA)
    SDL_RenderClear(gameRenderer);
    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer,
        backgroundTexture,
        NULL, NULL);
    if (bombState == NORMAL)
        SDL_RenderCopy(gameRenderer,
            bombTexture,
            &unexploded,
            &targetRectangle);
    else
        SDL_RenderCopy(gameRenderer,
            bombTexture,
            &exploded,
            &targetRectangle);
    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);
} // big while!


//Clean up!
SDL_DestroyTexture(playerTexture);
playerTexture = nullptr;
SDL_DestroyTexture(backgroundTexture);
backgroundTexture = nullptr;
SDL_DestroyRenderer(gameRenderer);
gameRenderer = nullptr;
SDL_DestroyWindow(gameWindow);
gameWindow = nullptr;
//Shutdown SDL - clear up resources etc.
SDL_Quit();
// Exit the program
exit(0);
}
